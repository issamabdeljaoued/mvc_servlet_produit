package test;

import java.util.List;

import metier.CatalogMetierImplementation;
import metier.entities.Produit;

public class Test {

	public static void main(String[] args) {
		CatalogMetierImplementation catalog=new CatalogMetierImplementation();
		catalog.addProduit(new Produit("HP",1520.120,5));
		catalog.addProduit(new Produit("DELL",1250,1));
		catalog.addProduit(new Produit("Asuse",1980.990,3));
		catalog.addProduit(new Produit("AlienWare",2500,4));
		
		List<Produit> prods=catalog.listProduits();
		for(Produit p: prods)
		{
			System.out.println(p.getDesignation());
		}
		List<Produit> prodcherche=catalog.getProduitParMC("H");
		for(Produit p: prodcherche)
		{
			System.out.println(p.getDesignation());
		}
	

	}

}
