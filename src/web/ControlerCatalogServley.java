package web;
import java.io.IOException;

import javax.persistence.metamodel.SetAttribute;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.CatalogMetierImplementation;
import metier.ICatalogMetier;
import metier.entities.Produit;

public class ControlerCatalogServley extends HttpServlet {
	private ICatalogMetier metier;
@Override
public void init() throws ServletException {
	 metier = new CatalogMetierImplementation();

}
@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
	doPost(request, response);
		
	}

@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		if(action!=null){
		if(action.equals("Ajouter")){
			try {
			String des=request.getParameter("deseingantion");
			double px = Double.parseDouble( request.getParameter("prix"));
			
			int quantite=Integer.parseInt(request.getParameter("quantite"));
			metier.addProduit(new Produit (des, px, quantite));
			} catch (Exception e) {
				request.setAttribute("exception", "erreur de saisie");
			}
		}
		else if(action.equals("supp")){
			Long id = Long.parseLong(request.getParameter("id"));
			
			metier.deleteProduit(id);
		}
		else if(action.equals("edit")){
			Long id = Long.parseLong(request.getParameter("id"));
			Produit p =metier.getProduit(id);
			request.setAttribute("produit", p);
		}

			if(action.equals("Update")){
				try {
				Long idP = Long.parseLong(request.getParameter("id"));
				String des=request.getParameter("deseingantion");
				double px = Double.parseDouble( request.getParameter("prix"));
				int quantite=Integer.parseInt(request.getParameter("quantite"));
				Produit p = new Produit(des, px, quantite);
				p.setIdProduit(idP);
			    metier.updateProduit(p);
			
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("exception", "erreur");
		}
			}
			
		}
		request.setAttribute("Produits", metier.listProduits());
		request.getRequestDispatcher("view/Produit.jsp").forward(request, response);
		
		
		
	}
}
