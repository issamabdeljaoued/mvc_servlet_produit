package metier;

import java.util.List;

import metier.entities.Produit;

public interface ICatalogMetier {
	public void addProduit(Produit p);
	public List<Produit> listProduits() ;
	public Produit getProduit(Long idProduit);
	public List<Produit> getProduitParMC(String mc);
	public void deleteProduit(Long idProduit);
	public void updateProduit(Produit p);

}
