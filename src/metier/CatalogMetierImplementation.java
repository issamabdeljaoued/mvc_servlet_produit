package metier;

import java.util.List;

import org.hibernate.*;
import org.hibernate.Query;

import org.hibernate.Session;

import metier.entities.Produit;
import util.HibernateUtil;

public class CatalogMetierImplementation implements ICatalogMetier {

	@Override
	public void addProduit(Produit p) {
		//Creation d(un objet session hibernate pour commenc� une transaction 
		// (pour faire une op�ration dans une base de donn�es faut une transaction)
		Session session=HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		try {
			session.save(p);
		} catch (Exception e) {
		    session.getTransaction().rollback();
			e.printStackTrace();
		}
		session.getTransaction().commit();
		
	}

	@Override
	public List<Produit> listProduits() {
		Session session=HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		//ex�cusion d'une requete pour r�cup�rer les produit
		Query req = session.createQuery("select p from Produit p");
		List<Produit> prods = req.list();
		session.getTransaction().commit();
		
		return prods;
	}

	@Override
	public Produit getProduit(Long idProduit) {
		Session session=HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		//ex�cusion d'une requete pour r�cup�rer les produit
		Object p = session.get(Produit.class, idProduit);
		if(p==null) throw new RuntimeException("Produit introuvable");
		session.getTransaction().commit();
		return (Produit)p;
	}

	@Override
	public List<Produit> getProduitParMC(String mc) {
		Session session=HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		//ex�cusion d'une requete pour r�cup�rer les produit
		Query req = session.createQuery("select p from Produit p where p.designation like :param");
		req.setParameter("param", "%"+mc+"%");
		List<Produit> prods = req.list();
		session.getTransaction().commit();
		return prods;
	}

	@Override
	public void deleteProduit(Long idProduit) {
		Session session=HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		//on charge le produit
		Object p = session.get(Produit.class, idProduit);
		if(p==null) throw new RuntimeException("Produit introuvable");
		session.delete(p);
		session.getTransaction().commit();
		
	}

	@Override
	public void updateProduit(Produit p) {
		Session session=HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		session.update(p);
		session.getTransaction().commit();
		
	}

}
